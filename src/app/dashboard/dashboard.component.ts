import { Component, OnInit } from '@angular/core';
import { ClientService } from '../services/client.service';
import { LocaleService } from '../services/locale.service';
import { ReservationService } from '../services/reservation.service';
import { VehiculeService } from '../services/vehicule.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: [
  ]
})
export class DashboardComponent implements OnInit {

  clientCount:any = "";
  locCount:any = "";
  vehCount:any = "";
  resCount:any = "";
  constructor(  public clientService: ClientService,public locService: LocaleService,public vehService: VehiculeService,public resService: ReservationService) { }

  ngOnInit(): void {
    this.numberOfClients();
    this.numberOfLocales();
  
    this.numberOfReservation();
  }
  numberOfClients(){
    this.clientService.numberOfClients().subscribe(data => {
      this.clientCount = data;
    });
  }
  numberOfLocales(){
    this.locService.numberOfLocales().subscribe(data => {
      this.locCount = data;
    });
  }
 
  numberOfReservation(){
    this.resService.numberOfRes().subscribe(data => {
      this.resCount = data;
    });
  }

}
